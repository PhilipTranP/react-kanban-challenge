import React, { Component } from 'react';

class Controls extends Component {

  render() {
    const { value, isBackDisable, isFowardDisable, onClickBack, onClickNext } = this.props
    return (
      <div style={{ padding: '1rem', background: '#D6F3FF' }}>
        <h1>Controls</h1>
        <div style={{ display: 'flex', marginTop: '1rem' }}>
          <input
            readOnly
            placeholder="Selected task name"
            style={{ fontSize: '1rem' }}
            data-testid="selected-task-field"
            value={value}
          />
          <button
            style={{ marginLeft: '1rem' }}
            disabled={isBackDisable}
            data-testid="move-back-btn"
            onClick={() => { onClickBack() }}
          >
            Move back
          </button>
          <button
            style={{ marginLeft: '1rem' }}
            disabled={isFowardDisable}
            data-testid="move-forward-btn"
            onClick={() => { onClickNext() }}
          >
            Move forward
          </button>
        </div>
      </div>
    )
  }
}

export default Controls;
