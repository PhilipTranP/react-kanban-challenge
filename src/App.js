import React, { Component } from 'react';
import './App.css';

import Controls from './components/Controls';
import Board from './components/Board';

const NUM_STAGES = 4;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [
        { name: 'task 0', stage: 0 },
        { name: 'task 1', stage: 0 },
        { name: 'task 2', stage: 0 },
        { name: 'task 3', stage: 0 },
        { name: 'task 4', stage: 1 },
        { name: 'task 5', stage: 1 },
        { name: 'task 6', stage: 1 },
        { name: 'task 7', stage: 2 },
        { name: 'task 8', stage: 2 },
        { name: 'task 9', stage: 3 },
      ],
      value: '',
      index: -1,
      stageId: 0,
      isBackDisable: true,
      isFowardDisable: true
    };
    this.stagesNames = ['Backlog', 'To Do', 'Ongoing', 'Done'];
  }

  hanleClick(name, stageId, task) {

    this.setState({
      value: name,
      isBackDisable: stageId === 0 ? true : false,
      isFowardDisable: stageId === 3 ? true : false,
      index: task.index
    })
  }

  handleBack() {

    let { index, stageId, tasks } = this.state

    if (index > -1) {
      const newtask = tasks[index]
      let newIndex = -1
      let tmpIndex = -1
      tasks.forEach((item, itemIndex) => {
        if (newtask.stage - 1 === item.stage && newIndex < itemIndex) {
          newIndex = itemIndex
        }
      })
      newtask.stage = newtask.stage - 1 >= 0 ? newtask.stage - 1 : 0
      stageId = newtask.stage
      const newTasks = []
      tasks.forEach((item, itemIndex) => {

        if (itemIndex !== index) {
          newTasks.push(item)
          if (itemIndex === newIndex) {
            newTasks.push(newtask)
            tmpIndex = newTasks.length - 1
          }

        }



      })

      index = newIndex
      if (tmpIndex > -1) {
        index = tmpIndex
      }

      this.setState(
        {
          tasks: newTasks,
          stageId,
          index,
          isBackDisable: stageId === 0,
          isFowardDisable: stageId >= 3
        }
      )
    }
  }

  handleNext() {
    let { index, stageId, tasks } = this.state

    if (index > -1) {
      const newtask = tasks[index]
      let newIndex = -1

      tasks.forEach((item, itemIndex) => {
        if (newtask.stage + 1 === item.stage && newIndex < itemIndex) {
          newIndex = itemIndex
        }
      })
      newtask.stage = newtask.stage + 1 < 4 ? newtask.stage + 1 : 3
      stageId = newtask.stage
      const newTasks = []
      tasks.forEach((item, itemIndex) => {
        if (itemIndex !== index) {
          newTasks.push(item)
        }
        if (itemIndex === newIndex) {
          newTasks.push(newtask)
        }

      })

      index = newIndex
      this.setState(
        {
          tasks: newTasks,
          stageId,
          index,
          isBackDisable: stageId === 0,
          isFowardDisable: stageId >= 3
        }
      )
    }
  }

  render() {
    const { tasks, value, isBackDisable, isFowardDisable } = this.state;

    let stagesTasks = [];
    for (let i = 0; i < NUM_STAGES; ++i) {
      stagesTasks.push([]);
    }
    tasks.forEach((task, index) => {
      task.index = index
      const stageId = task.stage;
      stagesTasks[stageId].push(task);
    })

    return (
      <div className="App">
        <Controls
          value={value}
          isBackDisable={isBackDisable}
          isFowardDisable={isFowardDisable}
          onClickBack={() => { this.handleBack() }}
          onClickNext={() => { this.handleNext() }}
        />
        <Board
          stagesTasks={stagesTasks}
          stagesNames={this.stagesNames}
          onClick={(name, stageId, task) => { this.hanleClick(name, stageId, task) }}
        />
      </div>
    );
  }
}

export default App;
